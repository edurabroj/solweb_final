<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri = "http://www.springframework.org/tags/form" %>

<t:layout>
    <jsp:body>
     	<h2>Propiedad</h2>
		<form:form method="post" commandName="objeto">			
			<div>
				<form:label path="nombre">Nombre</form:label>
				<form:input path="nombre" cssClass="form-control"/>
				<form:errors path="nombre"></form:errors>
			</div>			
			<div>
				<form:label path="direccion">Dirección</form:label>
				<form:input path="direccion" cssClass="form-control"/>
				<form:errors path="direccion"></form:errors>
			</div> <br>	
			<div>
				<input type="submit" value="Guardar" class="btn btn-success"/>
			</div>		
		</form:form>
    </jsp:body>
</t:layout>