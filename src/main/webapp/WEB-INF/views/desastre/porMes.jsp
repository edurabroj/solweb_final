<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri = "http://www.springframework.org/tags/form" %>

<t:layout>	
    <jsp:body>   
     	<h2>Desastres por mes</h2>   	
     	
    	<div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Mes</th>
						<th>Nro. de Desastres</th>
					</tr>
				</thead>				
				<tbody>			
					<c:forEach var = "item" items="${lista}">
						<c:if test="${item.nroDesastres>0}">
							<tr>
								<td><c:out value = "${item.mes}"/></td>
								<td><c:out value = "${item.nroDesastres}"/></td>
							</tr>
						</c:if>
			      	</c:forEach>
				</tbody>
			</table>			
				
			<c:if test = "${lista.size()==0}">
			    <h3 class="text-center">No se encontraron registros</h3>
			</c:if>
    	</div>
   		
   		<script>
	   		$('#confirm-delete').on('show.bs.modal', function(e) {
	   		    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	   		});
   		</script>
   		
    	<style>
    		.table td,th {
			   text-align: center;   
			}
    	</style>
    </jsp:body>
</t:layout>