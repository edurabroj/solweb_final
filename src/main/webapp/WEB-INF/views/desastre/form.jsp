<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri = "http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<t:layout>
    <jsp:body>    
     	<h2>Alquiler</h2>
		<form:form method="post" commandName="objeto">		
			<div>
				<form:label path="fecha">Fecha</form:label>
				<fmt:formatDate value="${objeto.fecha}" var="dateString" pattern="MM/dd/yyyy" />
				<form:input path="fecha" value="${dateString}" cssClass="form-control datepicker"/>
				<form:errors path="fecha"></form:errors>
			</div> 		
			<div>
				<form:label path="tipo">Tipo</form:label>
				<form:input path="tipo" cssClass="form-control"/>
				<form:errors path="tipo"></form:errors>
			</div> 	
			<div>
				<form:label path="distrito">Distrito</form:label>
				<form:input path="distrito" cssClass="form-control"/>
				<form:errors path="distrito"></form:errors>
			</div> 	
			<div>
				<form:label path="nroDamn">Número de damnificados</form:label>
				<form:input path="nroDamn" cssClass="form-control"/>
				<form:errors path="nroDamn"></form:errors>
			</div> 	<br>	
			<div>
				<input type="submit" value="Guardar" class="btn btn-success"/>
			</div>		
		</form:form>
		
		<script>
			$('.datepicker').datepicker({
			    format: 'mm/dd/yyyy'
			});
		</script>
    </jsp:body>
</t:layout>