<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri = "http://www.springframework.org/tags/form" %>

<t:layout>	
    <jsp:body>   
     	<h2>Desastres</h2>
    	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <h3>Eliminar desastre</h3>
		            </div>
		            <div class="modal-body">
		                ¿Desea eliminar el registro seleccionado? Esta acción es irreversible.
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
		                <a class="btn btn-danger btn-ok">Eliminar</a>
		            </div>
		        </div>
		    </div>
		</div>
    	
    	<div>
    		<a href="<c:url value="/desastre/agregar"/>" class="btn btn-success">Agregar  <span class="glyphicon glyphicon-plus"></span></a>
    	</div><br>
    	<div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>Tipo</th>
						<th>Distrito</th>
						<th>Nro. de Damnificados</th>
					</tr>
				</thead>				
				<tbody>			
					<c:forEach var = "item" items="${lista}">
						<tr>
							<td><c:out value = "${item.fecha}"/></td>
							<td><c:out value = "${item.tipo}"/></td>
							<td><c:out value = "${item.distrito}"/></td>
							<td><c:out value = "${item.nroDamn}"/></td>
						</tr>
			      	</c:forEach>
				</tbody>
			</table>			
				
			<c:if test = "${lista.size()==0}">
			    <h3 class="text-center">No se encontraron registros</h3>
			</c:if>
    	</div>
   		
   		<script>
	   		$('#confirm-delete').on('show.bs.modal', function(e) {
	   		    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
	   		});
   		</script>
   		
    	<style>
    		.table td,th {
			   text-align: center;   
			}
    	</style>
    </jsp:body>
</t:layout>