package com.solweb.proyecto.entities;

import java.util.Date;

public class Alquiler {
	private Long id;
	private Inquilino inquilino;
	private Propiedad propiedad;
	private Date fechaInicio;
	private float mensualidad;
	private int duracionMeses;	
	
	public Alquiler() {
		super();
	}

	public Alquiler(Long id, Inquilino inquilino, Propiedad propiedad, Date fechaInicio, float mensualidad,
			int duracionMeses) {
		super();
		this.id = id;
		this.inquilino = inquilino;
		this.propiedad = propiedad;
		this.fechaInicio = fechaInicio;
		this.mensualidad = mensualidad;
		this.duracionMeses = duracionMeses;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Inquilino getInquilino() {
		return inquilino;
	}

	public void setInquilino(Inquilino inquilino) {
		this.inquilino = inquilino;
	}

	public Propiedad getPropiedad() {
		return propiedad;
	}

	public void setPropiedad(Propiedad propiedad) {
		this.propiedad = propiedad;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public float getMensualidad() {
		return mensualidad;
	}

	public void setMensualidad(float mensualidad) {
		this.mensualidad = mensualidad;
	}

	public int getDuracionMeses() {
		return duracionMeses;
	}

	public void setDuracionMeses(int duracionMeses) {
		this.duracionMeses = duracionMeses;
	}

}
