package com.solweb.proyecto.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.solweb.proyecto.data.IDao;
import com.solweb.proyecto.entities.Inquilino;

@Service
public class ServiceInquilino {
	@Autowired
	private IDao dao;
	
	public void persist(Inquilino entity) {
		dao.persist(entity);
	}

	public List<Inquilino> getAll(String... searchCriterias){
		String dni = searchCriterias[0];
		String nombre = searchCriterias[1];
		String aPaterno = searchCriterias[2];
		String aMaterno = searchCriterias[3];
		
		List<Inquilino> list = dao.getAll(Inquilino.class);
		
		List<Inquilino> filtro;
		
		if(dni!=null && !dni.isEmpty()) {
			filtro = new ArrayList<>();
			
			for(Inquilino i: list) {
				if(i.getDni().toLowerCase().equals(dni.toLowerCase())) {
					filtro.add(i);
				}
			}
			
			list = filtro;
		}else {
			if(nombre!=null && !nombre.isEmpty()) {
				filtro = new ArrayList<>();
				
				for(Inquilino i: list) {
					if(i.getNombre().toLowerCase().contains(nombre.toLowerCase())) {
						filtro.add(i);
					}
				}
				
				list = filtro;
			}
			if(aPaterno!=null && !aPaterno.isEmpty()) {
				filtro = new ArrayList<>();
				
				for(Inquilino i: list) {
					if(i.getaPaterno().toLowerCase().contains(aPaterno.toLowerCase())) {
						filtro.add(i);
					}
				}
				
				list = filtro;
			}
			if(aMaterno!=null && !aMaterno.isEmpty()) {
				filtro = new ArrayList<>();
				
				for(Inquilino i: list) {
					if(i.getaMaterno().toLowerCase().contains(aMaterno.toLowerCase())) {
						filtro.add(i);
					}
				}
				
				list = filtro;
			}
		}
		return list;
	}
	public void delete(Serializable id) {
		dao.delete(Inquilino.class, id);
	}
	public Inquilino load(Serializable id) {
		return dao.load(Inquilino.class, id);
	}
	public Inquilino get(Serializable id) {
		return dao.get(Inquilino.class, id);
	}
	
	public List<Inquilino> getAll() {
		return dao.getAll(Inquilino.class);
	}
	
	public boolean dniYaRegistrado(Inquilino inquilino) {
		return dao.find("from Inquilino as i where i.dni = '" + inquilino.getDni() + "' and i.id != " + inquilino.getId() ).size() > 0;
	}
}
