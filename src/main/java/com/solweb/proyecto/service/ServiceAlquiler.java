package com.solweb.proyecto.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.solweb.proyecto.data.IDao;
import com.solweb.proyecto.entities.Alquiler;

@Service
public class ServiceAlquiler {
	@Autowired
	private IDao dao;
	
	public void persist(Alquiler entity) {
		dao.persist(entity);
	}

	public List<Alquiler> getAll(Long... searchCriterias){
		Long inquilinoId = searchCriterias[0];
		Long propiedadId = searchCriterias[1];
		
		List<Alquiler> list = dao.getAll(Alquiler.class);
		
		List<Alquiler> filtro;		
		
		/*if(inquilinoId>0) {
			filtro = new ArrayList<>();
			
			for(Alquiler p: list) {
				if(p.getInquilino_id()==inquilinoId) {
					filtro.add(p);
				}
			}
			
			list = filtro;
		}
		if(propiedadId>0) {
			filtro = new ArrayList<>();
			
			for(Alquiler p: list) {
				if(p.getPropiedad_id()==propiedadId) {
					filtro.add(p);
				}
			}
			
			list = filtro;
		}*/
		return list;
	}
	public void delete(Serializable id) {
		dao.delete(Alquiler.class, id);
	}
	public Alquiler load(Serializable id) {
		return dao.load(Alquiler.class, id);
	}
	public Alquiler get(Serializable id) {
		return dao.get(Alquiler.class, id);
	}
	
	public List<Alquiler> getAll() {
		return dao.getAll(Alquiler.class);
	}
}