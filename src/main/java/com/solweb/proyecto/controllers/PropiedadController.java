package com.solweb.proyecto.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.solweb.proyecto.entities.Inquilino;
import com.solweb.proyecto.entities.Propiedad;
import com.solweb.proyecto.service.ServicePropiedad;
import com.solweb.proyecto.validators.InquilinoValidator;
import com.solweb.proyecto.validators.PropiedadValidator;

@RequestMapping("propiedad")
@Controller
public class PropiedadController {
	private final String viewsUrl = "propiedad/", redirectUrl = "redirect:/propiedad";
	
	@Autowired
	private PropiedadValidator propiedadValidator;
	
	@Autowired
	private ServicePropiedad servicePropiedad;
	
	@RequestMapping("")
	public ModelAndView index(HttpServletRequest req) 
	{
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "index");
		
		String nombre = req.getParameter("nombre");
		String direccion = req.getParameter("direccion");
		mav.addObject("lista", servicePropiedad.getAll(nombre,direccion));
		
		mav.addObject("nombre", nombre);
		mav.addObject("direccion", direccion);
		return mav;
	}	
	
	@RequestMapping(value="/agregar", method=RequestMethod.GET)
	public ModelAndView agregar(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "form");
		mav.addObject("objeto", new Propiedad());
		return mav;
	}
	
	@RequestMapping(value="/agregar", method=RequestMethod.POST)
	public ModelAndView agregar
		(
				@ModelAttribute ("objeto") Propiedad objeto,
				BindingResult result
		)
	{
		propiedadValidator.validate(objeto, result);
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(viewsUrl + "form");
			mav.addObject("objeto", objeto);
			return mav;		
		}else {
			servicePropiedad.persist(objeto);
			return new ModelAndView(redirectUrl);
		}
	}
	
	@RequestMapping(value="/editar", method=RequestMethod.GET)
	public ModelAndView editar(HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "form");
		
		Long id = Long.parseLong(request.getParameter("id"));
		mav.addObject("objeto", servicePropiedad.get(id));
		return mav;
	}
	
	@RequestMapping(value="/editar", method=RequestMethod.POST)
	public ModelAndView editar
	(
		@ModelAttribute ("objeto") Propiedad objeto,
		BindingResult result
	)
	{
		propiedadValidator.validate(objeto, result);
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(viewsUrl + "form");
			mav.addObject("objeto", objeto);
			return mav;		
		}else {
			servicePropiedad.persist(objeto);
			return new ModelAndView(redirectUrl);
		}
	}
	
	@RequestMapping(value="/eliminar", method=RequestMethod.GET)
	public ModelAndView eliminar(HttpServletRequest request)
	{
		Long id = Long.parseLong(request.getParameter("id"));
		servicePropiedad.delete(id);
		return new ModelAndView(redirectUrl);
	}
}
