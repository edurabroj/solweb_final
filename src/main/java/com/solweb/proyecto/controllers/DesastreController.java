package com.solweb.proyecto.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.solweb.proyecto.data.IDao;
import com.solweb.proyecto.entities.Desastre;
import com.solweb.proyecto.validators.DesastreValidator;

import viewModels.DesastreMes;

import com.solweb.proyecto.entities.Desastre;

@RequestMapping("desastre")
@Controller
public class DesastreController {
	@Autowired
	private IDao dao;
	
	@Autowired
	private DesastreValidator desastreValidator;
	
	private final String viewsUrl = "desastre/", redirectUrl = "redirect:/desastre";

	
	HashMap<Integer,String> dict;
	
	public DesastreController() {
		dict = new HashMap<Integer,String>();
		dict.put(1, "Enero");
		dict.put(2, "Febrero");
		dict.put(3, "Marzo");
		dict.put(4, "Abril");
		dict.put(5, "Mayo");
		dict.put(6, "Junio");
		dict.put(7, "Julio");
		dict.put(8, "Agosto");
		dict.put(9, "Setiembre");
		dict.put(10, "Octubre");
		dict.put(11, "Noviembre");
		dict.put(12, "Diciembre");
	}
	
	@RequestMapping("")
	public ModelAndView index(HttpServletRequest req) 
	{
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "index");
		mav.addObject("lista", dao.getAll(Desastre.class));
		return mav;
	}	
	
	@RequestMapping("ordenados")
	public ModelAndView ordenados(HttpServletRequest req) 
	{
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "index");
		List<Desastre> listaDesastres = dao.find("from Desastre desastre order by desastre.distrito, desastre.fecha asc");	
		mav.addObject("lista", listaDesastres);	
		return mav;
	}	
	
	@RequestMapping("porMes")
	public ModelAndView porMes(HttpServletRequest req) 
	{
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "porMes");
		List<Desastre> listaDesastres = dao.getAll(Desastre.class);

		List<DesastreMes> lista = new ArrayList<DesastreMes>();
		
		for(int i=1; i<=12; i++) {
			lista.add( getDesastresMesByMes(listaDesastres,i));
		}
		
		mav.addObject("lista", lista);
		return mav;
	}
	
	private DesastreMes getDesastresMesByMes(List<Desastre> listaDesastres, int i) {
		int nro=0;
		for(Desastre d: listaDesastres) {
			if(d.getFecha().getMonth()+1==i) {
				nro++;
			}
		}
		return new DesastreMes( dict.get(i) ,nro);
	}

	@RequestMapping(value="/agregar", method=RequestMethod.GET)
	public ModelAndView agregar(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "form");
		mav.addObject("objeto", new Desastre());
		return mav;
	}
	
	@RequestMapping(value="/agregar", method=RequestMethod.POST)
	public ModelAndView agregar
		(
				@ModelAttribute ("objeto") Desastre objeto,
				BindingResult result
		)
	{
		desastreValidator.validate(objeto, result);
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(viewsUrl + "form");
			mav.addObject("objeto", objeto);
			return mav;		
		}else {
			dao.persist(objeto);
			return new ModelAndView(redirectUrl);
		}
	}	
}
