package com.solweb.proyecto.controllers;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.solweb.proyecto.entities.Alquiler;
import com.solweb.proyecto.entities.Inquilino;
import com.solweb.proyecto.service.ServiceAlquiler;
import com.solweb.proyecto.service.ServiceInquilino;
import com.solweb.proyecto.service.ServicePropiedad;
import com.solweb.proyecto.validators.AlquilerValidator;

@RequestMapping("alquiler")
@Controller
public class AlquilerController {
	private final String viewsUrl = "alquiler/", redirectUrl = "redirect:/alquiler";
	
	@Autowired
	private AlquilerValidator alquilerValidator;
	
	@Autowired
	private ServiceAlquiler serviceAlquiler;

	@Autowired
	private ServicePropiedad servicePropiedad;

	@Autowired
	private ServiceInquilino serviceInquilino;
	
	@RequestMapping("")
	public ModelAndView index(HttpServletRequest req) 
	{
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "index");
		
		/*String inquilinoId = req.getParameter("inquilinoId");
		String propiedadId = req.getParameter("propiedadId");
		
		mav.addObject("lista", serviceAlquiler.getAll((inquilinoId==null || inquilinoId.isEmpty()) ? 0 : Long.parseLong(inquilinoId),
														(propiedadId==null || propiedadId.isEmpty()) ? 0 : Long.parseLong(propiedadId)));
		*/
		
		mav.addObject("lista", serviceAlquiler.getAll());
		return mav;
	}	
	
	@RequestMapping(value="/agregar", method=RequestMethod.GET)
	public ModelAndView agregar(){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "form");
		mav.addObject("objeto", new Alquiler());

		mav.addObject("inquilinos", serviceInquilino.getAll());
		mav.addObject("propiedades", servicePropiedad.getAll());
		return mav;
	}
	
	@RequestMapping(value="/agregar", method=RequestMethod.POST)
	public ModelAndView agregar
		(
				@ModelAttribute ("objeto") Alquiler objeto,
				BindingResult result
		)
	{
		alquilerValidator.validate(objeto, result);
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(viewsUrl + "form");
			mav.addObject("objeto", objeto);
			
			mav.addObject("inquilinos", serviceInquilino.getAll());
			mav.addObject("propiedades", servicePropiedad.getAll());
			return mav;		
		}else {
			serviceAlquiler.persist(objeto);
			return new ModelAndView(redirectUrl);
		}
	}
	
	@RequestMapping(value="/editar", method=RequestMethod.GET)
	public ModelAndView editar(HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		mav.setViewName(viewsUrl + "form");
		
		Long id = Long.parseLong(request.getParameter("id"));
		mav.addObject("objeto", serviceAlquiler.get(id));

		mav.addObject("inquilinos", serviceInquilino.getAll());
		mav.addObject("propiedades", servicePropiedad.getAll());
		return mav;
	}
	
	@RequestMapping(value="/editar", method=RequestMethod.POST)
	public ModelAndView editar
	(
		@ModelAttribute ("objeto") Alquiler objeto,
		BindingResult result
	)
	{
		alquilerValidator.validate(objeto, result);
		if(result.hasErrors()) {
			ModelAndView mav = new ModelAndView();
			mav.setViewName(viewsUrl + "form");
			mav.addObject("objeto", objeto);

			mav.addObject("inquilinos", serviceInquilino.getAll());
			mav.addObject("propiedades", servicePropiedad.getAll());
			return mav;		
		}else {
			serviceAlquiler.persist(objeto);
			return new ModelAndView(redirectUrl);
		}
	}
	
	@RequestMapping(value="/eliminar", method=RequestMethod.GET)
	public ModelAndView eliminar(HttpServletRequest request)
	{
		Long id = Long.parseLong(request.getParameter("id"));
		serviceAlquiler.delete(id);
		return new ModelAndView(redirectUrl);
	}
}
