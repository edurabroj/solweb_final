package com.solweb.proyecto.validators;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.solweb.proyecto.entities.Alquiler;
import com.solweb.proyecto.entities.Propiedad;

@Component
public class AlquilerValidator implements Validator {	
	@Override
	public boolean supports(Class<?> type) {
		return Alquiler.class.isAssignableFrom(type);
	}

	@Override
	public void validate(Object o, Errors errors) {
		Alquiler alquiler = (Alquiler) o;		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "inquilino.id", "required.inquilino","Debe seleccionar un inquilino");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propiedad.id", "required.propiedad.id","Debe seleccionar una propiedad");		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fechaInicio", "required.fechaInicio","La fecha de inicio es obligatoria");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mensualidad", "required.mensualidad","La mensualidad es obligatoria");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "duracionMeses", "required.duracionMeses","La duraci�n es obligatoria");
		
		/*if(alquiler.getInquilino_id()<1) {
			errors.rejectValue("inquilino_id", "inquilino_id.null", "Escoja un iquilino");
		}
		if(alquiler.getPropiedad_id()<1) {
			errors.rejectValue("propiedad_id", "propiedad_id.null", "Escoja una propiedad");
		}*/
	}
}
