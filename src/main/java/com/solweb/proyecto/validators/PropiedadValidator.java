package com.solweb.proyecto.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.solweb.proyecto.entities.Propiedad;

@Component
public class PropiedadValidator implements Validator {	
	@Override
	public boolean supports(Class<?> type) {
		return Propiedad.class.isAssignableFrom(type);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre", "required.nombre","El nombre es obligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "direccion", "required.direccion","La direcci�n es obligatoria");
	}
}
