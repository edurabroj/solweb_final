package com.solweb.proyecto.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.solweb.proyecto.entities.Desastre;
import com.solweb.proyecto.entities.Propiedad;

public class DesastreValidator implements Validator {	
	@Override
	public boolean supports(Class<?> type) {
		return Desastre.class.isAssignableFrom(type);
	}

	@Override
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fecha", "required.nombre","La fecha es obligatoria");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tipo", "required.tipo","El tipo es obligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "distrito", "required.distrito","El distrito es obligatorio");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nroDamn", "required.nroDamn","El n�mero de damnificados es obligatorio");
	}
}